'use strict';

describe('Controller: ResumeController', function () {

    // load the controller's module
    beforeEach(module('cvdbClientApp'));

    it('should map routes to ResumeListController',
        inject(function (_$route_) {
            var $route = _$route_;

            expect($route.routes['/resume'].redirectTo).toBe('/resume/list');
            expect($route.routes['/resume/list'].templateUrl).toEqual('/views/resume/list.html');
            expect($route.routes['/resume/list'].controller).toBe('ResumeListController');
        })
    );

    it('should map routes to ResumeShowController and resolve model',
        inject(function (_$route_, _$injector_, _ResumeService_) {
            var $route = _$route_;
            var $injector = _$injector_;
            var ResumeService = _ResumeService_;
            spyOn(ResumeService, 'get').and.callFake(function (arg) {
                return { $promise: arg };
            });
            $route.current = {
                params: { id: '123' }
            };

            expect($route.routes['/resume/show/:id'].templateUrl).toBe('/views/resume/show.html');
            expect($route.routes['/resume/show/:id'].controller).toBe('ResumeShowController');
            var resolveModel = $route.routes['/resume/show/:id'].resolve.model;
            var model = $injector.invoke(resolveModel);
            expect(ResumeService.get).toHaveBeenCalledWith('123');
            expect(model).toBe('123');
        })
    );

    it('should map routes to ResumeEditController and resolve model',
        inject(function (_$route_, _$injector_, _ResumeService_) {
            var $route = _$route_;
            var $injector = _$injector_;
            var ResumeService = _ResumeService_;
            spyOn(ResumeService, 'get').and.callFake(function (arg) {
                return { $promise: arg };
            });
            $route.current = {
                params: { id: '123' }
            };

            expect($route.routes['/resume/edit/:id'].templateUrl).toBe('/views/resume/edit.html');
            expect($route.routes['/resume/edit/:id'].controller).toBe('ResumeEditController');
            var resolveModel = $route.routes['/resume/edit/:id'].resolve.model;
            var model = $injector.invoke(resolveModel);
            expect(ResumeService.get).toHaveBeenCalledWith('123');
            expect(model).toBe('123');
        })
    );

    it('should map routes to ResumeCreateController',
        inject(function (_$route_) {
            var $route = _$route_;

            expect($route.routes['/resume/create'].templateUrl).toEqual('/views/resume/edit.html');
            expect($route.routes['/resume/create'].controller).toBe('ResumeCreateController');
        })
    );


    it('should load resumes in ResumeListController',
        inject(function ($rootScope, $controller, ResumeService, $q) {
            var scope = $rootScope.$new();
            spyOn(ResumeService, 'getSearchString').and.callFake(function () {
                return '';
            });
            spyOn(ResumeService, 'getStoredPage').and.callFake(function () {
                return 0;
            });
            spyOn(ResumeService, 'setStoredPage');
            var result = {
                content: [
                    {
                        id: 1,
                        firstName: 'first',
                        lastName: 'last'
                    }
                ]
            };
            spyOn(ResumeService, 'list').and.callFake(function () {
                var deferred = $q.defer();
                deferred.resolve(result);
                return { $promise: deferred.promise };
            });
            $controller('ResumeListController', {
                $scope: scope
            });
            scope.$digest();
            expect(ResumeService.getSearchString).toHaveBeenCalled();
            expect(ResumeService.getStoredPage).toHaveBeenCalled();
            expect(scope.columns.length).toBe(2);
            expect(scope.columns[0].name).toBe('firstName');
            expect(scope.columns[0].label).toBe('First Name');
            expect(scope.columns[0].sortable).toBe(true);
            expect(scope.columns[1].name).toBe('lastName');
            expect(scope.columns[1].label).toBe('Last Name');
            expect(scope.columns[1].sortable).toBe(true);
            expect(scope.sort).toBe('firstName');
            expect(scope.order).toBe('asc');
            expect(scope.itemsPerPage).toBe(10);
            expect(scope.maxSize).toBe(6);
            expect(scope.model.content[0].id).toBe(1);
            expect(scope.model.content[0].firstName).toBe('first');
            expect(scope.model.content[0].lastName).toBe('last');
            expect(ResumeService.setStoredPage).toHaveBeenCalledWith(0);
            expect(scope.currentPage).toBe(1);
        })
    );

    it('should navigate to edit in ResumeShowController',
        inject(function ($rootScope, $controller, $location, $routeParams) {
            var scope = $rootScope.$new();
            $routeParams.id = 123;
            spyOn($location, 'path');
            $controller('ResumeShowController', {
                $scope: scope,
                model: 'testModel'
            });
            scope.edit();
            expect($location.path).toHaveBeenCalledWith('/resume/edit/123');
        })
    );

    it('should remove items in ResumeShowController',
        inject(function ($rootScope, $controller, $location, $q, ConfirmationService, ResumeService, toaster) {
            var scope = $rootScope.$new();
            var model = {
                id: 123,
                firstName: 'first',
                lastName: 'last'
            };
            spyOn(ConfirmationService, 'confirm').and.callFake(function() {
                var deferred = $q.defer();
                deferred.resolve();
                return deferred.promise;
            });
            spyOn(ResumeService, 'remove').and.callFake(function() {
                var deferred = $q.defer();
                deferred.resolve();
                return { $promise: deferred.promise };
            });
            spyOn(toaster, 'pop');
            spyOn($location, 'path');
            $controller('ResumeShowController', {
                $scope: scope,
                model: model
            });
            scope.remove();
            scope.$digest();
            expect(ConfirmationService.confirm).toHaveBeenCalledWith('Deletion of Resume', 'Are you sure you want to delete the resume of first last?');
            expect(ResumeService.remove).toHaveBeenCalledWith(123);
            expect(toaster.pop).toHaveBeenCalledWith('success', null, 'Resume has been deleted');
            expect($location.path).toHaveBeenCalledWith('/resume/list');
        })
    );

    it('should save items in ResumeEditController',
        inject(function ($rootScope, $controller, $location, $q, ResumeService, toaster) {
            var scope = $rootScope.$new();
            var model = {
                id: 123,
                firstName: 'first',
                lastName: 'last'
            };
            spyOn(ResumeService, 'save').and.callFake(function() {
                var deferred = $q.defer();
                deferred.resolve({ id: 456 });
                return { $promise: deferred.promise };
            });
            spyOn(toaster, 'pop');
            spyOn($location, 'path');
            $controller('ResumeEditController', {
                $scope: scope,
                model: model
            });
            scope.save();
            scope.$digest();
            expect(ResumeService.save).toHaveBeenCalledWith(model);
            expect(toaster.pop).toHaveBeenCalledWith('success', null, 'Resume has been saved');
            expect($location.path).toHaveBeenCalledWith('/resume/show/456');
        })
    );

    it('should save items in ResumeCreateController',
        inject(function ($rootScope, $controller, $location, $q, ResumeService, toaster) {
            var scope = $rootScope.$new();
            spyOn(ResumeService, 'save').and.callFake(function() {
                var deferred = $q.defer();
                deferred.resolve({ id: 456 });
                return { $promise: deferred.promise };
            });
            spyOn(toaster, 'pop');
            spyOn($location, 'path');
            $controller('ResumeCreateController', {
                $scope: scope
            });
            scope.model.firstName = 'first';
            scope.model.lastName = 'last';
            scope.save();
            scope.$digest();
            expect(ResumeService.save).toHaveBeenCalledWith(scope.model);
            expect(toaster.pop).toHaveBeenCalledWith('success', null, 'Resume has been created');
            expect($location.path).toHaveBeenCalledWith('/resume/show/456');
        })
    );

});
