'use strict';

describe('Controller: HeaderController', function () {

    // load the controller's module
    beforeEach(module('cvdbClientApp'));

    it('should hideNavbar if path starts with /auth',
        inject(function ($rootScope, $controller, _$location_) {
            var scope = $rootScope.$new();
            $controller('HeaderController', {
                $scope: scope
            });
            spyOn(_$location_, 'path').and.callFake(function () {
                return '/auth/dummy';
            });
            expect(scope.hideNavbar()).toBe(true);
        })
    );

    it('should not hideNavbar if path does not start with /auth',
        inject(function ($rootScope, $controller, _$location_) {
            var scope = $rootScope.$new();
            $controller('HeaderController', {
                $scope: scope
            });
            spyOn(_$location_, 'path').and.callFake(function () {
                return 'x/auth/dummy';
            });
            expect(scope.hideNavbar()).toBe(false);
        })
    );

    it('should return active for navbarActive if location path equals argument',
        inject(function ($rootScope, $controller, _$location_) {
            var scope = $rootScope.$new();
            $controller('HeaderController', {
                $scope: scope
            });
            spyOn(_$location_, 'path').and.callFake(function () {
                return '/resume';
            });
            expect(scope.navbarActive('/resume')).toBe('active');
        })
    );

    it('should return active for navbarActive if location path starts with argument followed by slash',
        inject(function ($rootScope, $controller, _$location_) {
            var scope = $rootScope.$new();
            $controller('HeaderController', {
                $scope: scope
            });
            spyOn(_$location_, 'path').and.callFake(function () {
                return '/resume/list';
            });
            expect(scope.navbarActive('/resume')).toBe('active');
        })
    );

    it('should return empty String otherwise for navbarActive',
        inject(function ($rootScope, $controller, _$location_) {
            var scope = $rootScope.$new();
            $controller('HeaderController', {
                $scope: scope
            });
            spyOn(_$location_, 'path').and.callFake(function () {
                return '/resumeTest';
            });
            expect(scope.navbarActive('/resume')).toBe('');
        })
    );
});
