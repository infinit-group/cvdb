'use strict';

describe('Controller: AuthenticationController', function () {

    // load the controller's module
    beforeEach(module('cvdbClientApp'));

    it('should map routes to controllers and resolve model',
        inject(function (_$route_, _$injector_, _AuthenticationService_) {
            var $route = _$route_;
            var $injector = _$injector_;
            var AuthenticationService = _AuthenticationService_;
            spyOn(AuthenticationService, 'logout');
            expect($route.routes['/auth/login'].controller).toBe('LoginController');
            expect($route.routes['/auth/login'].templateUrl).toEqual('/views/auth/login.html');
            expect($route.routes['/auth/logout'].controller).toBe('LogoutController');
            expect($route.routes['/auth/logout'].template).toBe('');
            var resolveModel = $route.routes['/auth/logout'].resolve.model;
            $injector.invoke(resolveModel);
            expect(AuthenticationService.logout).toHaveBeenCalled();
        })
    );

    it('should initialize LoginController',
        inject(function ($rootScope, $controller) {
            var scope = $rootScope.$new();
            $controller('LoginController', {
                $scope: scope
            });
            expect(scope.model.username).toBe('');
            expect(scope.model.password).toBe('');
            expect(scope.model.rememberMe).toBe(false);
            expect(scope.loginHasBeenCalled).toBe(false);
        })
    );

    it('should do nothing if no credentials provided on login',
        inject(function ($rootScope, $controller) {
            var scope = $rootScope.$new();
            $controller('LoginController', {
                $scope: scope
            });
            var result = scope.login();
            expect(result).toBeUndefined();
            expect(scope.loginHasBeenCalled).toBe(true);
        })
    );

    it('should do successful login',
        inject(function ($rootScope, $controller, AuthenticationService, $q, _$location_) {
            var scope = $rootScope.$new();
            $controller('LoginController', {
                $scope: scope,
                $location: _$location_
            });
            scope.model.username = 'foo';
            scope.model.password = 'bar';
            scope.model.rememberMe = false;
            spyOn(AuthenticationService, 'login').and.callFake(function () {
                var deferred = $q.defer();
                deferred.resolve();
                return deferred.promise;
            });
            spyOn(_$location_, 'path').and.callThrough();
            scope.login();
            scope.$digest();
            expect(_$location_.path).toHaveBeenCalledWith('/resume/list');
        })
    );

    it('should reject incorrect credentials',
        inject(function ($rootScope, $controller, AuthenticationService, $q, _toaster_) {
            var scope = $rootScope.$new();
            $controller('LoginController', {
                $scope: scope
            });
            scope.model.username = 'foo';
            scope.model.password = 'bar';
            scope.model.rememberMe = false;
            spyOn(AuthenticationService, 'login').and.callFake(function () {
                var deferred = $q.defer();
                deferred.reject({status: 401});
                return deferred.promise;
            });
            spyOn(_toaster_, 'pop');
            scope.login();
            scope.$digest();
            expect(_toaster_.pop).toHaveBeenCalledWith('error', 'Login failed', 'Incorrect User/Password');
        })
    );

    it('should set path on logout',
        inject(function ($rootScope, $controller, _$location_) {
            var scope = $rootScope.$new();
            spyOn(_$location_, 'path');
            $controller('LogoutController', {
                $scope: scope,
                $location: _$location_
            });
            expect(_$location_.path).toHaveBeenCalledWith('/auth/login');
        })
    );
});
