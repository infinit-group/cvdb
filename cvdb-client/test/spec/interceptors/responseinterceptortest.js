'use strict';

describe('Interceptor: ResponseInterceptor', function () {

    var interceptors;

    // load the controller's module
    beforeEach(module('cvdbClientApp', function ($httpProvider) {
        // The httpProvider can only be injected in the configuration phase
        interceptors = $httpProvider.interceptors;
    }));

    it('should be registered with httpProvider',
        inject(function () {
            expect(interceptors).toContain('responseInterceptor');
        })
    );

    it('should path through successful response',
        inject(function (responseInterceptor) {
            var responseMock = { status: 200 };
            expect(responseInterceptor.response(responseMock)).toEqual(responseMock);
        })
    );

    it('should redirect to login on unauthorized response',
        inject(function (responseInterceptor, $location, $q) {
            spyOn($location, 'search').and.callFake(function() {
                return {};
            });
            var searchSpy = jasmine.createSpy('search');
            spyOn($location, 'path').and.callFake(function(args) {
                if(args === undefined) {
                    // fake the path getter
                    return '/fakePath';
                } else {
                    return { search: searchSpy };
                }
            });
            spyOn($q, 'reject').and.callFake(function(args) {
                return args;
            });
            var responseMock = { status: 401 };
            var result = responseInterceptor.responseError(responseMock);
            expect(result).toEqual(responseMock);
            expect($q.reject).toHaveBeenCalledWith(responseMock);
            expect($location.path).toHaveBeenCalledWith('/auth/login');
            expect(searchSpy).toHaveBeenCalledWith('returnTo', '/fakePath');
        })
    );

    it('should notify user on error',
        inject(function (responseInterceptor, toaster, $q) {
            spyOn(toaster, 'pop');
            spyOn($q, 'reject').and.callFake(function(args) {
                return args;
            });
            var responseMock = { status: 500, statusText: 'Fake Failure' };
            var result = responseInterceptor.responseError(responseMock);
            expect(result).toEqual(responseMock);
            expect($q.reject).toHaveBeenCalledWith(responseMock);
            expect(toaster.pop).toHaveBeenCalledWith('error', 'Error processing request', 'Fake Failure [500]');
        })
    );

});
