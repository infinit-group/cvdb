'use strict';

angular.module('cvdbClientApp')

    .config(['$routeProvider', function ($routeProvider) {

        var templatePrefix = '/views/auth';
        $routeProvider
            .when('/auth/login', {
                templateUrl: templatePrefix + '/login.html',
                controller: 'LoginController'
            })
            .when('/auth/logout', {
                template: '',
                controller: 'LogoutController',
                resolve: {
                    model: ['AuthenticationService', function (AuthenticationService) {
                        return AuthenticationService.logout();
                    }]
                }
            });

    }])

    .controller('LoginController', ['$scope', '$location', 'AuthenticationService', 'toaster', function ($scope, $location, AuthenticationService, toaster) {
        $scope.model = {
            username: '',
            password: '',
            rememberMe: false
        };

        $scope.loginHasBeenCalled = false;

        $scope.login = function () {
            $scope.loginHasBeenCalled = true;
            if (!$scope.model.username || !$scope.model.password) {
                return;
            }
            AuthenticationService.login($scope.model.username, $scope.model.password, $scope.model.rememberMe)
                .then(function () {
                    var redirectTo = $location.search().returnTo;
                    if (redirectTo === undefined) {
                        redirectTo = AuthenticationService.getDefaultRedirection();
                    }
                    $location.path(redirectTo).search({});
                }, function (rejection) {
                    if (rejection.status === 401) {
                        toaster.pop('error', 'Login failed', 'Incorrect User/Password');
                    } else {
                        toaster.pop('error', 'Error processing request', rejection.statusText + ' [' + rejection.status + ']');
                    }
                });
        };
    }])


    .controller('LogoutController', ['$location', function ($location) {
        $location.path('/auth/login');
    }]);
