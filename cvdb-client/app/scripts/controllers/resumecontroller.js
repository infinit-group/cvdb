'use strict';

angular.module('cvdbClientApp')

    .config(['$routeProvider', function ($routeProvider) {

        var templatePrefix = '/views/resume';
        $routeProvider
            .when('/resume', {
                redirectTo: '/resume/list'
            })
            .when('/resume/list', {
                templateUrl: templatePrefix + '/list.html',
                controller: 'ResumeListController'
            })
            .when('/resume/show/:id', {
                templateUrl: templatePrefix + '/show.html',
                controller: 'ResumeShowController',
                resolve: {
                    model: ['$route', 'ResumeService', function ($route, ResumeService) {
                        return ResumeService.get($route.current.params.id).$promise;
                    }]
                }
            })
            .when('/resume/edit/:id', {
                templateUrl: templatePrefix + '/edit.html',
                controller: 'ResumeEditController',
                resolve: {
                    model: ['$route', 'ResumeService', function ($route, ResumeService) {
                        return ResumeService.get($route.current.params.id).$promise;
                    }]
                }
            })
            .when('/resume/create', {
                templateUrl: templatePrefix + '/edit.html',
                controller: 'ResumeCreateController'
            });

    }])

    .controller('ResumeListController', ['$scope', '$location', 'ResumeService', function ($scope, $location, ResumeService) {

        // private methods
        var gotoFirstPage = function () {
            ResumeService.setStoredPage(0);
            $scope.onSelectPage(ResumeService.getStoredPage() + 1);
        };

        // "Add Resume" button
        $scope.create = function () {
            $location.path('/resume/create');
        };

        // "Search" input field
        $scope.searchString = ResumeService.getSearchString();
        $scope.$watch('searchString', function (newValue) {
            ResumeService.setSearchString(newValue);
            gotoFirstPage();
        });

        // table definition
        $scope.columns = [
            {
                name: 'firstName',
                label: 'First Name',
                sortable: true
            },
            {
                name: 'lastName',
                label: 'Last Name',
                sortable: true
            }
        ];
        $scope.sort = 'firstName';
        $scope.order = 'asc';
        $scope.itemsPerPage = 10;
        $scope.maxSize = 6;

        $scope.$watch('sort', function (newValue) {
            ResumeService.setSort(newValue);
            gotoFirstPage();
        });

        $scope.$watch('order', function (newValue) {
            ResumeService.setOrder(newValue);
            gotoFirstPage();
        });

        $scope.onRowClicked = function (row) {
            $location.path('/resume/show/' + row.id);
        };

        $scope.onSelectPage = function (page) {
            ResumeService.list(page - 1, $scope.itemsPerPage).$promise.then(function (result) {
                $scope.model = result;
                ResumeService.setStoredPage(result.number);
                $scope.currentPage = ResumeService.getStoredPage() + 1;
                if (!$scope.$$phase) {
                    $scope.$apply();
                }
            });
        };
        $scope.onSelectPage(ResumeService.getStoredPage() + 1);

    }])

    .controller('ResumeShowController', ['$scope', '$location', '$routeParams', 'ResumeService', 'ConfirmationService', 'model', 'toaster', function ($scope, $location, $routeParams, ResumeService, ConfirmationService, model, toaster) {

        $scope.model = model;

        $scope.edit = function () {
            $location.path('/resume/edit/' + $routeParams.id);
        };

        $scope.remove = function () {
            var confirmQuestion = 'Are you sure you want to delete the resume of ' +
                $scope.model.firstName + ' ' + $scope.model.lastName + '?';
            ConfirmationService.confirm('Deletion of Resume', confirmQuestion).then(function () {
                ResumeService.remove($scope.model.id).$promise.then(function () {
                    toaster.pop('success', null, 'Resume has been deleted');
                    // ResumeService.setStoredPage(0);
                    $location.path('/resume/list');
                });
            });
        };

        $scope.back = function () {
            $location.path('/resume/list');
        };

    }])

    .controller('ResumeEditController', ['$scope', '$location', '$routeParams', 'ResumeService', 'model', 'toaster', function ($scope, $location, $routeParams, ResumeService, model, toaster) {

        $scope.model = model;

        $scope.cancel = function () {
            $location.path('/resume/show/' + $routeParams.id);
        };

        $scope.save = function () {
            ResumeService.save($scope.model).$promise.then(function (model) {
                toaster.pop('success', null, 'Resume has been saved');
                $location.path('/resume/show/' + model.id);
            });
        };

    }])

    .controller('ResumeCreateController', ['$scope', '$location', '$routeParams', 'ResumeService', 'toaster', function ($scope, $location, $routeParams, ResumeService, toaster) {

        $scope.model = {};

        $scope.cancel = function () {
            $location.path('/resume/list');
        };

        $scope.save = function () {
            ResumeService.save($scope.model).$promise.then(function (model) {
                toaster.pop('success', null, 'Resume has been created');
                $location.path('/resume/show/' + model.id);
            });
        };
    }]);
