'use strict';

angular.module('cvdbClientApp')
    .directive('myTable', function () {
        return {
            templateUrl: '/views/directives/mytable.html',
            restrict: 'E',
            scope: {
                columns: '=',      // list of maps each containing column.name, column.label and column.sortable
                sort: '=',         // column.name used for sorting
                order: '=',        // sort order: 'asc' or 'desc'
                rows: '=',         // current page: list of maps each mapping column.name onto a cell value
                onRowClicked: '&'    // function called when a row was clicked passing the row as parameter
            },
            controller: ['$scope', function ($scope) {

                // sort and order
                $scope.isSortedBy = function (columnName) {
                    return $scope.sort === columnName;
                };

                $scope.isOrder = function (order) {
                    return $scope.order === order;
                };

                // header
                $scope.onHeaderClicked = function (column) {
                    if (!column.sortable) {
                        return;
                    }
                    if ($scope.sort === column.name) {
                        $scope.order = $scope.order === 'asc' ? 'desc' : 'asc';
                    } else {
                        $scope.sort = column.name;
                        $scope.order = 'asc';
                    }
                };

                // row
                $scope._onRowClicked = function (row) {
                    $scope.onRowClicked({row: row});
                };

            }]
        };
    });
