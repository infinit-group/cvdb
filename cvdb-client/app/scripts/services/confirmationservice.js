'use strict';

angular.module('cvdbClientApp')

		.controller('ConfirmationController', ['$scope', '$modalInstance', 'model', function ($scope, $modalInstance, model) {

			$scope.model = model;

			$scope.ok = function () {
				$modalInstance.close();
			};

			$scope.cancel = function () {
				$modalInstance.dismiss();
			};
		}])

		.service('ConfirmationService', ['$modal', function ($modal) {
			this.confirm = function (header, body) {
				var modalInstance = $modal.open({
					templateUrl: '/views/confirmation/confirmation.html',
					controller: 'ConfirmationController',
					resolve: {
						model: function () {
							return {
								'header': header,
								'body': body
							};
						}
					}
				});
				return modalInstance.result;
			};
		}]);