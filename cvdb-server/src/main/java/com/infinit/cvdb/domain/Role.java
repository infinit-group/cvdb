package com.infinit.cvdb.domain;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

/**
 * Role of the user that can login into the system
 *
 * Created by owahlen on 05.01.14.
 */
@Entity
@Cacheable
public class Role {

	@Id
	@GeneratedValue
	private Long id;

	@Column(nullable = false, unique = true)
	String authority;

	String description;

	public Long getId() {
		return id;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
