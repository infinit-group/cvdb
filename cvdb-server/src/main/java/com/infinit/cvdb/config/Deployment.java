package com.infinit.cvdb.config;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

/**
 * Servlet 3.0+ environments allow to replace the web.xml file with a programmatic configuration.
 *
 * Created by owahlen on 01.01.14.
 */
public class Deployment extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

}
