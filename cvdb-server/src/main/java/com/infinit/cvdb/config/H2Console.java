package com.infinit.cvdb.config;

import org.h2.server.web.WebServlet;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * H2 Console
 * Created by owahlen on 29.07.14.
 */
@Configuration
public class H2Console {

	/**
	 * Define the H2 Console Servlet
	 * @return ServletRegistrationBean to be processed by Spring
	 */
	@Bean
	public ServletRegistrationBean h2servletRegistration() {
		ServletRegistrationBean registration = new ServletRegistrationBean(new WebServlet());
		registration.addUrlMappings("/console/*");
		return registration;
	}

}
