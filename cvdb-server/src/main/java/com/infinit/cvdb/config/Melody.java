package com.infinit.cvdb.config;

import net.bull.javamelody.MonitoringFilter;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Melody monitoring configuration
 * Created by owahlen on 29.07.14.
 */
@Configuration
// fixme: Proxying of the datasource currently throws an exception. Therefore spring integration is commented out:
// @ImportResource("classpath:/net/bull/javamelody/monitoring-spring.xml")
public class Melody {

	/**
	 * Define the melody Filter
	 * @return FilterRegistrationBean to be processed by Spring
	 */
	@Bean
	public FilterRegistrationBean melodyFilterRegistrationBean() {
		MonitoringFilter monitoringFilter = new MonitoringFilter();
		FilterRegistrationBean registrationBean = new FilterRegistrationBean(monitoringFilter);
		registrationBean.addServletNames("monitoring");
		registrationBean.addUrlPatterns("/*");
		registrationBean.setMatchAfter(true);
		return registrationBean;
	}

}
