package com.infinit.cvdb.repository;

import com.infinit.cvdb.domain.Resume;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * DAO for the Resume Entity.
 * Spring data implements all method of the interfaces automatically through proxies.
 *
 * Created by owahlen on 31.12.13.
 */
public interface ResumeRepository extends PagingAndSortingRepository<Resume, Long>, QueryDslPredicateExecutor<Resume>, ResumeRepositoryCustom {
}
