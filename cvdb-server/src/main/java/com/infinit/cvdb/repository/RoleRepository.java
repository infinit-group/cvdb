package com.infinit.cvdb.repository;

import com.infinit.cvdb.domain.Role;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * DAO for the Role Entity
 *
 * Created by owahlen on 31.12.13.
 */
public interface RoleRepository extends PagingAndSortingRepository<Role, Long> {

	Role findByAuthority(String authority);

}
