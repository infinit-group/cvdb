package com.infinit.cvdb.repository;

import com.infinit.cvdb.domain.QResume;
import com.infinit.cvdb.domain.Resume;
import com.mysema.query.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

/**
 * Implementation of ResumeRepositoryCustom
 *
 * Created by owahlen on 17.04.14.
 */
@Component // The @Component annotation is only needed to suppress warnings in Intellij
public class ResumeRepositoryImpl implements ResumeRepositoryCustom {

	@Autowired
	private ResumeRepository resumeRepository;

	@Override
	public Page<Resume> search(String search, Pageable pageable) {
		QResume resume = QResume.resume;
		Predicate predicate = resume.firstName.containsIgnoreCase(search).or(resume.lastName.containsIgnoreCase(search));
		return resumeRepository.findAll(predicate, pageable);
	}
}
