package com.infinit.cvdb.repository;

import com.infinit.cvdb.domain.User;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * DAO for the User Entity exposing Rest Endpoint
 *
 * Created by owahlen on 31.12.13.
 */
public interface UserRepository extends PagingAndSortingRepository<User, Long> {

	User findByUsername(String username);

}
