package com.infinit.cvdb.controller;

import com.infinit.cvdb.domain.Resume;
import com.infinit.cvdb.repository.ResumeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

/**
 * Resume RestController
 * Created by owahlen on 10.04.14.
 */
@RestController
@RequestMapping("/api/resume")
public class ResumeController {

	@Autowired
	ResumeRepository resumeRepository;

	@RequestMapping(method = RequestMethod.GET)
	public Page<Resume> list(
			@RequestParam(value = "page", required = false, defaultValue = "0") int page,
			@RequestParam(value = "size", required = false, defaultValue = "20") int size,
			@RequestParam(value = "search", required = false, defaultValue = "") String search,
			@RequestParam(value = "sort", required = false, defaultValue = "firstName") String sortString,
			@RequestParam(value = "order", required = false, defaultValue = "asc") String orderString
	) {
		PageRequest pageRequest = new PageRequest(page, size, Sort.Direction.fromString(orderString), sortString);
		return resumeRepository.search(search, pageRequest);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Resume get(@PathVariable Long id) {
		return resumeRepository.findOne(id);
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public Resume post(@RequestBody Resume resume) {
		return resumeRepository.save(resume);
	}

	// Only Admins may delete a resume
	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		resumeRepository.delete(id);
	}

}

