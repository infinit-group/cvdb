package com.infinit.cvdb.config

import com.infinit.cvdb.domain.Role
import com.infinit.cvdb.domain.User
import com.infinit.cvdb.repository.RoleRepository
import com.infinit.cvdb.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationListener
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.core.Ordered
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

/**
 * Additional fixtures required for testing
 *
 * Created by owahlen on 31.12.13.
 */
@Component
@Transactional
class TestFixtures implements ApplicationListener<ContextRefreshedEvent>, Ordered {

	@Autowired
	PasswordEncoder passwordEncoder

	@Autowired
	UserRepository userRepository

	@Autowired
	RoleRepository roleRepository

	@Override
	void onApplicationEvent(ContextRefreshedEvent event) {
		if(!event.getApplicationContext().getParent()) {
			configure()
		}
	}

	/**
	 * Test fixtures shall always run after production fixtures
	 */
	@Override
	int getOrder() {
		return 30
	}

	private void configure() {
		Role userRole = roleRepository.findByAuthority('ROLE_USER')
		assert userRole

		User testUser = new User()
		testUser.setUsername('test')
		testUser.setPassword(passwordEncoder.encode("password"))
		testUser.setEnabled(true)
		testUser.addToRoles(userRole)
		userRepository.save(testUser)
	}
}
