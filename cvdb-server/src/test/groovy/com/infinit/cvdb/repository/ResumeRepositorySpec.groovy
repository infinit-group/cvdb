package com.infinit.cvdb.repository

import com.infinit.cvdb.domain.Resume
import com.infinit.cvdb.test.TestSpec
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import spock.lang.Unroll

/**
 * Created by owahlen on 31.12.13.
 */
class ResumeRepositorySpec extends TestSpec {

	@Autowired
	ResumeRepository resumeRepository

	@Unroll
	def "search for String '#searchString' should deliver #expectedNumberResults results"(String searchString, Integer expectedNumberResults) {
		setup:
		PageRequest pageRequest = new PageRequest(0, 1000, Sort.Direction.ASC, 'firstName')

		when:
		Page<Resume> resumes = resumeRepository.search(searchString, pageRequest)

		then:
		resumes
		resumes.totalElements == expectedNumberResults

		where:
		searchString || expectedNumberResults
		'xyz'        || 0
		'JOHN_999'   || 1
		'john_999'   || 1
		'john'       || 1000
		'DOE'        || 1000
		'doe'        || 1000
		'doe_1'      || 111  // _1, _10.._19, _100.._199
		'_99'        || 11   // _99, _990.._999
	}

}
