package com.infinit.cvdb.repository

import com.infinit.cvdb.domain.Role
import com.infinit.cvdb.test.TestSpec
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Unroll

/**
 * Created by owahlen on 31.12.13.
 */
class RoleRepositorySpec extends TestSpec {

	@Autowired
	RoleRepository roleRepository

	@Unroll
	def "findByAuthority should find Role '#roleName'"(String roleName) {
		when:
		Role role = roleRepository.findByAuthority(roleName)

		then:
		role
		role.authority == roleName

		where:
		roleName << ['ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_USER']
	}

}
