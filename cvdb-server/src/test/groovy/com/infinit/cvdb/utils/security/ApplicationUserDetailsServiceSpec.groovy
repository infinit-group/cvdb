package com.infinit.cvdb.utils.security

import com.infinit.cvdb.domain.User
import com.infinit.cvdb.repository.UserRepository
import com.infinit.cvdb.test.TestSpec
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import spock.lang.Unroll

/**
 * Created by owahlen on 31.12.13.
 */
class ApplicationUserDetailsServiceSpec extends TestSpec {

	@Autowired
	private UserDetailsService applicationUserDetailsService

	@Unroll
	def "User '#username' should have role '#expectedRoleName'"(String username, String expectedRoleName) {
		when:
		UserDetails userDetails = applicationUserDetailsService.loadUserByUsername(username)

		then:
		userDetails.authorities
		userDetails.authorities.size() == 1
		GrantedAuthority grantedAuthority = userDetails.authorities.iterator().next()
		grantedAuthority.authority == expectedRoleName

		where:
		username  || expectedRoleName
		'admin'   || 'ROLE_ADMIN'
		'manager' || 'ROLE_MANAGER'
		'user'    || 'ROLE_USER'
		'test'    || 'ROLE_USER'
	}

}
