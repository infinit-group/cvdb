package com.infinit.cvdb.controller

import com.infinit.cvdb.test.WebTestSpec
import org.apache.tomcat.util.codec.binary.Base64
import org.springframework.http.MediaType
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.web.context.HttpSessionSecurityContextRepository
import org.springframework.test.web.servlet.ResultActions

import static org.hamcrest.CoreMatchers.isA
import static org.hamcrest.CoreMatchers.nullValue
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * Tests for the Authentication Controller
 * Created by owahlen on 29.07.14.
 */
class AuthenticationControllerSpec extends WebTestSpec {

	private AuthenticationController authenticationController

	def setup() {
		authenticationController = new AuthenticationController()
	}

	def "login with proper credentials should create security context in session"() {
		setup:
		String basicDigestHeaderValue = createBasicDigestHeader('test','password')

		when:
		ResultActions result = mockMvc.perform(post('/api/authentication/login')
				.header('Authorization', basicDigestHeaderValue)
				.accept(MediaType.APPLICATION_JSON)
		)

		then:
		result.andExpect(status().isOk())
		result.andExpect(request().sessionAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, isA(SecurityContext)))
	}

	def "login without credentials should return HttpStatus.UNAUTHORIZED"() {
		when:
		ResultActions result = mockMvc.perform(post('/api/authentication/login'))

		then:
		result.andExpect(status().isUnauthorized())
	}

	def "login with incorrect credentials should return HttpStatus.UNAUTHORIZED"() {
		setup:
		String basicDigestHeaderValue = createBasicDigestHeader('test', 'incorrect_password')

		when:
		ResultActions result = mockMvc.perform(post('/api/authentication/login')
				.header('Authorization', basicDigestHeaderValue)
				.accept(MediaType.APPLICATION_JSON)
		)

		then:
		result.andExpect(status().isUnauthorized())
	}

	def "logout should invalidate security context in session"() {
		setup:
		String basicDigestHeaderValue = createBasicDigestHeader('test','password')

		when:
		ResultActions result = mockMvc.perform(post('/api/authentication/logout')
				.header('Authorization', basicDigestHeaderValue)
				.accept(MediaType.APPLICATION_JSON)
		)

		then:
		result.andExpect(status().isOk())
		result.andExpect(request().sessionAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, nullValue()))
	}

	private static String createBasicDigestHeader(String user, String password) {
		return 'Basic ' + new String(Base64.encodeBase64(("$user:$password").getBytes()))
	}

}
