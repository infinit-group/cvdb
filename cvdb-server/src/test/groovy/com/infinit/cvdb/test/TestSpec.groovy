package com.infinit.cvdb.test

import com.infinit.cvdb.config.TestApplication
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.transaction.annotation.Transactional
import spock.lang.Specification

/**
 * Base class for integration tests
 * Created by owahlen on 29.07.14.
 */
@ContextConfiguration(classes = TestApplication, loader = SpringApplicationContextLoader)
@ActiveProfiles("test")
@Transactional // rollback after each test method
abstract class TestSpec extends Specification {
}
